package br.com.rodrigo;

import br.com.rodrigo.Cambio;
import br.com.rodrigo.ConversorDeMoeda;
import org.junit.Assert;
import org.junit.Test;

public class ConversorDeMoedaTest {

    @Test
    public void
            deveConverterDeRealParaDolar() {

        ConversorDeMoeda conversor = new ConversorDeMoeda();
        double resultado = conversor.calcular(1, 1);

        Assert.assertEquals(2.5, resultado, 0.01);

    }

    {

    }

}
