/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo;

/**
 *
 * 
 */
public class Cambio {
    
    private double real;
    private double dolar;
    private double australiano;

    public Cambio() {
    }

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public double getDolar() {
        return dolar;
    }

    public void setDolar(double dolar) {
        this.dolar = dolar;
    }

    public double getAustraliano() {
        return australiano;
    }

    public void setAustraliano(double australiano) {
        this.australiano = australiano;
    }

    
    
}